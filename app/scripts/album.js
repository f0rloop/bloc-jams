var changeAlbumView = function  (album) {
	
	var $albumTitle = $('.album-title');
	$albumTitle.text(album.name);

	var $albumArtist = $('.album-artist');
	$albumArtist.text(album.artist);

	var $albumMeta = $('.album-meta');
	$albumArtist.text(album.year + " on " + album.label);

	var $albumImage = $('.album-image img');
	$albumImage.attr('src', album.albumArtUrl);

	var $songList = $('.album-song-listing');
	$songList.empty();
	var songs = album.songs;
	for (var i = 0; i < songs.length; i++) {
		var songData = songs[i];
		var $newRow = createSongRow(i + 1, songData.name, songData.length);
		$songList.append($newRow);
	}
};

var createSongRow = function (songNumber, songName, songLength) {
	var template =
		'<tr>'
	+	'	<td class="col-md-1">' + songNumber + '</td>'
	+	'	<td class="col-md-9">' + songName + '</td>'
	+	'	<td class="col-md-2">' + songLength + '</td>'
	+	'</tr>'
	;

	return $(template);
}


var albumPicasso = {
	name: 'The Colors',
	artist: 'Pablo Picasso',
	label: 'Cubism',
	year: '1881',
	albumArtUrl: '/images/album-placeholder.png',
	songs: [
		{ name: 'Blue', length: '4:26' },
		{ name: 'Green', length: '4:26' },
		{ name: 'Red', length: '4:26' },
		{ name: 'Pink', length: '4:26' },
		{ name: 'Magenta', length: '4:26' }
	]
};

var albumMarconi = {
	name: 'The Telephone',
	artist: 'Guglielmo Marconi',
	label: 'EM',
	year: '1989',
	albumArtUrl: '/images/album-placeholder.png',
	songs: [
		{ name: 'Hello, Operator?', length: '4:26' },
		{ name: 'Ring, ring, ring', length: '4:26' },
		{ name: 'Fits in your pocket', length: '4:26' },
		{ name: 'Can you hear me now?', length: '4:26' },
		{ name: 'Wrong phone number', length: '4:26' }
	]
};

if (document.URL.match(/\album.html/)) {
	$(document).ready(function() {
		changeAlbumView(albumPicasso);
	})
};